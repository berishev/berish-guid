import * as ringle from 'berish-ringle';

class Util {
  guid() {
    return (
      this.shortGuid() +
      this.shortGuid() +
      '-' +
      this.shortGuid() +
      '-' +
      this.shortGuid() +
      '-' +
      this.shortGuid() +
      '-' +
      this.shortGuid() +
      this.shortGuid() +
      this.shortGuid()
    );
  }

  shortGuid() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

  generateId() {
    return `${this.shortGuid()}${this.shortGuid()}`;
  }
}

export default ringle.getSingleton(Util);
